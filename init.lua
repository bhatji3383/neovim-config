--local cmd = vim.cmd
--local map = require('utils').map

require('plugins')
require('autocmds')

-- Arduino
--cmd 'augroup Arduino'
--cmd 'autocmd BufEnter src/main.cpp, src/main.c, src/main.h :set filetype=arduino'
--map('n', '<leader>r', ':!platformio -f -c nvim run<CR>')
--map('n', '<leader>u', ':!platformio -f -c nvim run --target upload<CR>')
--map('n', '<leader>c', ':!platformio -f -c nvim run --target clean<CR>')
--cmd 'augroup END'

require('settings')
require('keybinds')
require('statusline')
--require('lsp')
