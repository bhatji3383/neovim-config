/******************************************************************************
* File:             main.h
*
* Author:           Benjamin James  
* Created:          
* Description:      Main program header file which contains pin defenitions
*                   and important function declarations.
*****************************************************************************/

#ifndef __MAIN_H
#define __MAIN_H

#include <stm32f1xx_hal.h>

void Error_handler(void);
void SysClk_Config(void);

#endif
