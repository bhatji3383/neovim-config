local map = require('utils').map
local cmd = vim.cmd

map('', '<C-s>', ':w<CR>', {silent = true})
map('', '<C-x>', ':q!<CR>', {silent = true})
map('', '<C-M-x>', ':wqa!<CR>', {silent = true})
map('', '<C-t>', ':tabnew<CR>', {silent = true})
map('', '<M-t>', ':tabnew<CR>:terminal<CR>i<CR>', {silent = true})
cmd "tnoremap <Esc> <C-\\><C-n>"
map('n', '<M-s>', 'z=', {})
map('n', '<C-M-w>', ':mksession ~/.config/nvim/sessions/', {silent = true})
map('n', '<C-M-o>', ':source ~/.config/nvim/sessions/', {silent = true})
map('n', '<C-M-s>', ':Files<CR>', {silent = true})
map('', '<C-Right>', 'gt', {silent = true})
map('', '<C-Left>', 'gT', {silent = true})

map('n', 'gd', ':call CocAction("jumpDefinition", "tab drop")<CR>', {silent = true})
map('n', 'gy', ':call CocAction("jumpTypeDefinition", "tab drop")<CR>', {silent = true})
map('n', 'gm', ':call CocAction("jumpImplementation", "tab drop")<CR>', {silent = true})
map('n', 'gr', ':call CocAction("jumpRefrences", "tab drop")<CR>', {silent = true})

map('n', '<F5>', ':PresentingStart<CR>', {silent = true})
map('n', '<F6>', ':.!toilet -w 200 -f term -F border<CR>', {silent = true})
