local Autocmd = require('utils').Augroup

Autocmd({
    {'BufEnter', '*.tex', ':silent set wrap linebreak nolist spell'},
    {'BufWritePost', '*.tex', ':silent !bibtex %:t:r'},
    {'BufWritePost', '*.tex', ':silent !compiler %'}
  }, 'Latex')

Autocmd({
    {'BufWritePost *.scss,*.sass :silent !sass % %:r.css'}
  }, 'SCSS')

Autocmd({
    {'BufNewFile', 'sys.h', '0r ~/.config/nvim/templates/stm32f1xx_hal_sys_h_file.h'},
    {'BufNewFile', 'sys.c', '0r ~/.config/nvim/templates/stm32f1xx_hal_sys_c_file.c'},
    {'BufNewFile', 'main.c', '0r ~/.config/nvim/templates/stm32f1xx_main_c_file.c'},
    {'BufNewFile', 'main.h', '0r ~/.config/nvim/templates/stm32f1xx_main_h_file.h'}
  }, 'stm32_templates')
