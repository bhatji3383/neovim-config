local cmd = vim.cmd
cmd [[packadd packer.nvim]]
require('packer').startup(function()
  use {'wbthomason/packer.nvim', opt = true}
  use 'benjaminjamesxyz/darker'
  use {'tjdevries/nlua.nvim', event = 'VimEnter *.lua', ft = {'lua'}}
  use {'rafcamlet/coc-nvim-lua', event = 'VimEnter *.lua', ft={'lua'}}
  use {'epilande/vim-es2015-snippets', ft = {'javascript', 'javascriptreact', 'js', 'jsx'}}
  use {'epilande/vim-react-snippets', ft = {'javascript', 'javascriptreact', 'js', 'jsx'}}
  use {'neoclide/coc.nvim', branch = 'release'}
  use 'honza/vim-snippets'
  use 'SirVer/ultisnips'
  use 'townk/vim-autoclose'
  use 'norcalli/nvim-colorizer.lua'
  use 'sheerun/vim-polyglot'
  use 'junegunn/fzf.vim'
  use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
  use {'liuchengxu/vista.vim'}
  use {'sotte/presenting.vim'}
  --use 'neovim/nvim-lspconfig'
  --use 'nvim-lua/completion-nvim'
end)
