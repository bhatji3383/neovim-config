local cmd = vim.cmd
local o = vim.o
local g = vim.g

--Theme
o.termguicolors = true
o.guifont="FiraCode Nerd Font:h18"
o.background = 'dark'
cmd 'colorscheme Darker'

--General settings
cmd 'syntax on'
cmd 'set relativenumber'
vim.o.mouse='a'
cmd 'set clipboard=unnamedplus'
g.nrformats='alpha,bin,octal,hex'
o.updatetime=2000
o.timeoutlen=300
o.tabstop=1
o.softtabstop=2
o.shiftwidth=2
o.expandtab = true
o.autoindent = true
cmd 'set nobackup'
cmd 'set nowritebackup'
o.magic = true
o.sidescroll=0
g.guioptions='rb'
o.pumheight=20
cmd 'let mapleader = ","'

-- Spelling checking
g.spelllang='en_us'
g.spellfile='~/.config/nvim/spell/en.utf-8.add'

-- Python
g.python3_host_prog = "/usr/bin/python3"
g.python_host_prog = "/usr/bin/python2"

-- Treesitter
local ts = require 'nvim-treesitter.configs'
ts.setup { ensure_installed = 'maintained', highlight = { enable = true } }

-- Coc
cmd 'set shortmess+=c'
cmd 'set hidden'

--Vista
g.vista_sidebar_position='vertical topleft'
